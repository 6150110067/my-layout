package com.example.myprofile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class third_pro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_pro);
        Button button = findViewById(R.id.btnBack);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (third_pro.this,second_pro.class);
                startActivity(intent);
            }
        });


    }
    public void facebook (View v){
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/aneefah.salae"));
        startActivity(i);
    }
    public void instagram (View v){
        Intent s = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.instagram.com/n.nisqnix/"));
        startActivity(s);
    }
    public void youtube (View v){
        Intent d = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=JSAfPh1A25E"));
        startActivity(d);
    }
}